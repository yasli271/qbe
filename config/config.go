package config

import (
	"github.com/rs/zerolog"
)

type Config struct {
	APIName         string
	APIPort         string
	LogLevel        zerolog.Level
}
