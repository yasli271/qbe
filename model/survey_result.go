package model

import "github.com/satori/go.uuid"

type SurveyResult struct {
	TableName struct{} `sql:"qbe_survey_result" json:"-"`

	Id               int64
	ClickId          string
	UniqueId         uuid.UUID
	SurveyId         int64
	SurveyQuestionId int64
	SurveyAnswerId   int64
	Value            string
}

type SurveyResultForm struct {
	ClickId string              `json:"click_id"`
	Fields  []SurveyResultField `json:"fields"`
}

type SurveyResultField struct {
	SurveyQuestionId int64  `json:"question_id"`
	SurveyAnswerId   int64  `json:"answer_id"`
	Value            string `json:"value"`
}

func (obj *SurveyResultForm) ToSurveyResult(surveyId int64) (*[]SurveyResult, error) {
	unique, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}

	res := []SurveyResult{}
	for _, v := range obj.Fields {
		item := SurveyResult{
			ClickId:          obj.ClickId,
			UniqueId:         unique,
			SurveyId:         surveyId,
			SurveyQuestionId: v.SurveyQuestionId,
			SurveyAnswerId:   v.SurveyAnswerId,
			Value:            v.Value,
		}

		res = append(res, item)
	}

	return &res, nil
}
