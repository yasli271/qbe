package model

type QuestionSchema struct {
	Type    string
	Options struct {
		Hidden bool
		Min      int64 `json:",omitempty"`
		Max      int64 `json:",omitempty"`
		Children []QuestionSchemaChild `json:",omitempty"`
	} `json:",omitempty"`
}

type QuestionSchemaChild struct {
	AnswerIds   []int64 `json:",omitempty"`
	QuestionIds []int64 `json:",omitempty"`
}
