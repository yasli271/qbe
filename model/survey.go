package model

type Survey struct {
	TableName struct{} `sql:"qbe_survey" json:"-"`

	Id int64
	Country string
	Topic string
	Title string

	SurveyQuestions []*SurveyQuestion `pg:"fk:survey_id"`
}
