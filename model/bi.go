package model

import (
	"github.com/go-pg/pg/orm"
	"github.com/rs/zerolog/log"
)

func (e *Model) GetSurvey(id int64) (*Survey, error) {
	survey := Survey{
		Id: id,
	}

	err := e.db.Model(&survey).WherePK().First()
	if err != nil {
		return nil, err
	}

	questions := []*SurveyQuestion{}
	err = e.db.Model(&questions).Relation("SurveyAnswers", func(q *orm.Query) (*orm.Query, error) {
		return q.Order("survey_answer.order ASC"), nil
	}).Where("survey_id = ?", id).Order("survey_question.order ASC").Select()
	if err != nil {
		return nil, err
	}

	survey.SurveyQuestions = questions

	return &survey, nil
}

func (e *Model) SaveAnswers(data *[]SurveyResult) error {
	_, err := e.db.Model(data).Insert()
	return err
}

func (e *Model) GenerateQuestion() {
	q := new(SurveyQuestion)
	q.Order = 7
	q.SurveyId = 1
	q.Scheme = *new(QuestionSchema)
	q.Scheme.Type = "multi"
	q.Scheme.Options.Min = 2
	q.Scheme.Options.Max = 5
	c1 := QuestionSchemaChild{
		AnswerIds: []int64{6, 7},
		QuestionIds: []int64{4, 5},
	}

	c2 := QuestionSchemaChild{
		AnswerIds: []int64{4, 5},
		QuestionIds: []int64{3},
	}

	q.Scheme.Options.Children = []QuestionSchemaChild{c1, c2}

	_, err := e.db.Model(q).Insert()
	if err != nil {
		log.Error().Err(err).Msg("error saving")
	}
}