package model

type SurveyAnswer struct {
	TableName struct{} `sql:"qbe_survey_answer" json:"-"`

	Id int64
	SurveyQuestionId int64
	Order int
	Text string
}
