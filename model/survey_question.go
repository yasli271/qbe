package model

type SurveyQuestion struct {
	TableName struct{} `sql:"qbe_survey_question" json:"-"`

	Id int64
	SurveyId int64
	Order int64
	Text string
	Scheme QuestionSchema

	SurveyAnswers []*SurveyAnswer `pg:"fk:survey_question_id"`
}
