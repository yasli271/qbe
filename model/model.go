package model

import (
	"context"
	"errors"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type Model struct {
	db *pg.DB
}

type dbLogger struct{}

func (d dbLogger) BeforeQuery(ctx context.Context, q *pg.QueryEvent) (context.Context, error) {
	return ctx, nil
}

func (d dbLogger) AfterQuery(ctx context.Context, q *pg.QueryEvent) (context.Context, error) {
	query, _ := q.FormattedQuery()
	log.Debug().Msgf("%s", query)
	return ctx, nil
}

func New(db *pg.Options) (e *Model, err error) {
	if db == nil {
		return nil, errors.New("miss db config")
	}
	e = new(Model)
	//Check if DBs present and has tables
	e.db = pg.Connect(db)
	if zerolog.DebugLevel == zerolog.GlobalLevel() {
		e.db.AddQueryHook(dbLogger{})
	}

	_, err = e.db.Exec("SELECT * FROM pg_catalog.pg_tables;", nil)
	if err != nil {
		return nil, err
	}

	return e, nil
}

func (e *Model) CreateTables() (err error) {
	log.Warn().Msg("Create tables in DB if not exists.")
	op := orm.CreateTableOptions{
		IfNotExists:   true,
		Temp:          false,
		FKConstraints: true,
	}

	for _, model := range []interface{}{
		(*Survey)(nil),
		(*SurveyQuestion)(nil),
		(*SurveyAnswer)(nil),
		(*SurveyResult)(nil),
	} {
		err = e.db.CreateTable(model, &op)
		if err != nil {
			log.Error().Err(err).Msgf("Error creating table %s", model)
			return err
		}
	}

	return nil
}

func (e *Model) checkErr(err error) {
	if err != nil {
		log.Fatal().Err(err)
	}
}
