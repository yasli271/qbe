package main

import (
	"github.com/ABCMobilecom/QBE/config"
	"github.com/ABCMobilecom/QBE/service"
	"github.com/go-pg/pg"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigName("config")
	viper.AddConfigPath("./config")
}

func main() {
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().Err(err).Msg("")
	}
	var cfg config.Config
	err = viper.Sub("config").Unmarshal(&cfg)
	if err != nil {
		log.Fatal().Err(err).Msg("Config")
	}

	var db pg.Options
	err = viper.Sub("db").Unmarshal(&db)
	if err != nil {
		log.Fatal().Err(err).Msg("DB Config")
	}

	api, err := service.New(&cfg, &db)
	if err != nil {
		log.Fatal().Err(err).Msg("Service")
	}

	err = api.Run()
	if err != nil {
		log.Fatal().Err(err).Msg("Run")
	}
}
