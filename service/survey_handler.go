package service

import (
	"encoding/json"
	"github.com/ABCMobilecom/QBE/model"
	"github.com/rs/zerolog/log"
	"github.com/valyala/fasthttp"
	"strconv"
)

func (e *Service) SurveyHandler(ctx *fasthttp.RequestCtx) {
	_id := ctx.UserValue("id")
	id, err := strconv.ParseInt(_id.(string), 10, 8)
	if err != nil {
		log.Error().Err(err).Msgf("Error parsing SurveyId %v", _id)
	}

	survey, err := e.model.GetSurvey(id)
	if err != nil {
		log.Error().Err(err).Msgf("Error getting SurveyId %d", id)
		ctx.Response.SetStatusCode(fasthttp.StatusNotFound)
		ctx.Response.SetBody([]byte{})
		return
	}

	data, err := json.Marshal(&survey)

	ctx.Response.SetBody(data)
	ctx.Response.Header.SetContentType("application/json")
}

func (e *Service) SurveyPostHandler(ctx *fasthttp.RequestCtx) {
	_id := ctx.UserValue("id")
	id, err := strconv.ParseInt(_id.(string), 10, 8)
	if err != nil {
		log.Error().Err(err).Msgf("Error parsing SurveyId %v", _id)
		ctx.Error("", fasthttp.StatusBadRequest)
		return
	}

	body := ctx.PostBody()
	data := model.SurveyResultForm{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		log.Error().Err(err).Msg("Error parsing partner create/update request json.")
		ctx.Error("", fasthttp.StatusBadRequest)
		return
	}

	res, err := data.ToSurveyResult(id)
	if err != nil {
		log.Error().Err(err).RawJSON("form", body).Msgf("Error parsing form")
		ctx.Error("", fasthttp.StatusInternalServerError)
		return
	}

	err = e.model.SaveAnswers(res)
	if err != nil {
		log.Error().Err(err).RawJSON("form", body).Msgf("Error saving results")
		ctx.Error("", fasthttp.StatusInternalServerError)
		return
	}

	ctx.Response.SetStatusCode(fasthttp.StatusCreated)
}
