package service

import (
	"fmt"
	"github.com/valyala/fasthttp"
)

func (e *Service) Health(ctx *fasthttp.RequestCtx) {
	ctx.SuccessString("text/plain", fmt.Sprintf("OK"))
}

var (
	corsAllowHeaders     = "authorization"
	corsAllowMethods     = "HEAD,GET,POST,PUT,DELETE,OPTIONS"
	corsAllowOrigin      = "*"
	corsAllowCredentials = "true"
)

func (e *Service) CORS(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return fasthttp.RequestHandler(func(ctx *fasthttp.RequestCtx) {

		ctx.Response.Header.Set("Access-Control-Allow-Credentials", corsAllowCredentials)
		ctx.Response.Header.Set("Access-Control-Allow-Headers", corsAllowHeaders)
		ctx.Response.Header.Set("Access-Control-Allow-Methods", corsAllowMethods)
		ctx.Response.Header.Set("Access-Control-Allow-Origin", corsAllowOrigin)

		next(ctx)
	})
}

func (e *Service) OptionsHook(ctx *fasthttp.RequestCtx) {
	ctx.Response.Reset()
	ctx.Response.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody([]byte{})
}

func (e *Service) GenerateHook(ctx *fasthttp.RequestCtx) {
	e.model.GenerateQuestion()
}