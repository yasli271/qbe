package service

import (
	"errors"
	"github.com/ABCMobilecom/QBE/config"
	"github.com/ABCMobilecom/QBE/model"
	"github.com/buaazp/fasthttprouter"
	"github.com/go-pg/pg"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/valyala/fasthttp"
)

type Service struct {
	config *config.Config
	model  *model.Model
}

func New(config *config.Config, db *pg.Options) (service *Service, err error) {
	if config == nil {
		return nil, errors.New("wrong config")
	}

	if db == nil {
		return nil, errors.New("wrong db config")
	}

	zerolog.SetGlobalLevel(config.LogLevel)

	service = new(Service)
	service.config = config

	service.model, err = model.New(db)
	if err != nil {
		return nil, err
	}

	err = service.model.CreateTables()
	if err != nil {
		return nil, err
	}

	return service, nil
}

func (e *Service) Run() (err error) {
	router := fasthttprouter.New()
	router.GET("/health", e.Health)

	router.GET("/survey/:id", e.CORS(e.SurveyHandler))
	router.POST("/survey/:id", e.CORS(e.SurveyPostHandler))

	router.OPTIONS("/survey/:id", e.CORS(e.OptionsHook))

	log.Info().Msgf("API start on %s", e.config.APIPort)
	return fasthttp.ListenAndServe(e.config.APIPort, router.Handler)
}
